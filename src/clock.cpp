/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QDateTime>
#include <QDBusPendingCallWatcher>
#include <QDebug>

#include "qconnman_debug.h"
#include "clockinterface.h"
#include "clock_p.h"
#include "clock.h"

QHash<QString, Clock::UpdatePolicy> Clock::s_policyLookup;
Clock::Clock(QObject *parent)
    : ConnManObject(parent),
      d_ptr(new ClockPrivate)
{
    Q_D(Clock);
    if (s_policyLookup.isEmpty()) {
        s_policyLookup.insert(QLatin1String("auto"), Clock::AutoPolicy);
        s_policyLookup.insert(QLatin1String("manual"), Clock::ManualPolicy);
    }

    d->clockInterface =
        new ClockInterface("net.connman", "/", QDBusConnection::systemBus(), this);
    if (!d->clockInterface->isValid()) {
        qConnmanDebug() << Q_FUNC_INFO << "unable to connect to clock";
        return;
    }

    connect(d->clockInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                        this, SLOT(propertyChanged(QString,QDBusVariant)));

    // get initial properties
    QDBusPendingReply<QVariantMap> pReply = d->clockInterface->GetProperties();
    QDBusPendingCallWatcher *pWatcher = new QDBusPendingCallWatcher(pReply, this);
    connect(pWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(getPropertiesResponse(QDBusPendingCallWatcher*)));
    pWatcher->waitForFinished();
}

Clock::~Clock()
{
}

quint64 Clock::timeInternal() const
{
    Q_D(const Clock);
    return d->time;
}

QDateTime Clock::time() const
{
    Q_D(const Clock);
    return QDateTime::fromMSecsSinceEpoch(d->time * 1000);
}

void Clock::setTime(const QDateTime &time)
{
    Q_D(Clock);
    QDBusPendingReply<> reply =
        d->clockInterface->SetProperty("Time", QDBusVariant(time.toMSecsSinceEpoch() / 1000));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    } else {
        d->time = time.toMSecsSinceEpoch() / 1000;
    }
}

Clock::UpdatePolicy Clock::timeUpdates() const
{
    Q_D(const Clock);
    return s_policyLookup.value(d->timeUpdates);
}

void Clock::setTimeUpdates(UpdatePolicy policy)
{
    Q_D(Clock);
    QDBusPendingReply<> reply =
        d->clockInterface->SetProperty("TimeUpdates", QDBusVariant(s_policyLookup.key(policy)));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    } else {
        d->timeUpdates = s_policyLookup.key(policy);
    }
}

QString Clock::timezone() const
{
    Q_D(const Clock);
    return d->timezone;
}

void Clock::setTimezone(const QString &timezone)
{
    Q_D(Clock);
    QDBusPendingReply<> reply = d->clockInterface->SetProperty("Timezone", QDBusVariant(timezone));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    } else {
        d->timezone = timezone;
    }
}

Clock::UpdatePolicy Clock::timezoneUpdates() const
{
    Q_D(const Clock);
    return s_policyLookup.value(d->timezoneUpdates);
}

void Clock::setTimezoneUpdates(UpdatePolicy policy)
{
    Q_D(Clock);
    QDBusPendingReply<> reply =
        d->clockInterface->SetProperty("TimezoneUpdates", QDBusVariant(s_policyLookup.key(policy)));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    } else {
        d->timezoneUpdates = s_policyLookup.key(policy);
    }
}

QStringList Clock::timeservers() const
{
    Q_D(const Clock);
    return d->timeservers;
}

void Clock::setTimeservers(const QStringList &servers)
{
    Q_D(Clock);
    QDBusPendingReply<> reply = d->clockInterface->SetProperty("Timeservers", QDBusVariant(servers));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    } else {
        d->timeservers = servers;
    }
}

void Clock::getPropertiesResponse(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QVariantMap> reply = *call;
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error().message();
    } else {
        QVariantMap result = reply.value();
        foreach (QString property, result.keys())
            setObjectProperty(this, property, result.value(property));
    }

    call->deleteLater();
}

void Clock::setTimeInternal(quint64 time)
{
    Q_D(Clock);
    d->time = time;
    Q_EMIT dataChanged();
}

void Clock::setTimeUpdatesInternal(const QString &policy)
{
    Q_D(Clock);
    d->timeUpdates = policy;
    Q_EMIT dataChanged();
}

void Clock::setTimezoneInternal(const QString &timezone)
{
    Q_D(Clock);
    d->timezone = timezone;
    Q_EMIT dataChanged();
}

void Clock::setTimezoneUpdatesInternal(const QString &policy)
{
    Q_D(Clock);
    d->timezoneUpdates = policy;
    Q_EMIT dataChanged();
}

void Clock::setTimeserversInternal(const QStringList &servers)
{
    Q_D(Clock);
    d->timeservers = servers;
    Q_EMIT dataChanged();
}

QString Clock::timeUpdatesInternal() const
{
    Q_D(const Clock);
    return d->timeUpdates;
}

QString Clock::timezoneUpdatesInternal() const
{
    Q_D(const Clock);
    return d->timezoneUpdates;
}


