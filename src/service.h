/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef SERVICE_H
#define SERVICE_H

#include <QObject>
#include <QStringList>
#include <QDBusObjectPath>
#include <QDBusArgument>
#include <QScopedPointer>

#include "types.h"
#include "connmanobject.h"

class Service;
class ConfigurableObject : public QObject
{
    Q_OBJECT
public:
    explicit ConfigurableObject(Service *parent);
    void apply();

};

class IPV4DataPrivate;
class IPV4Data : public ConfigurableObject
{
    Q_OBJECT
    Q_PROPERTY(QString Method READ method WRITE setMethod)
    Q_PROPERTY(QString Address READ address WRITE setAddress)
    Q_PROPERTY(QString Netmask READ netmask WRITE setNetmask)
    Q_PROPERTY(QString Gateway READ gateway WRITE setGateway)
public:
    explicit IPV4Data(Service *parent);
    ~IPV4Data();

    QString method() const;
    void setMethod(const QString &method);

    QString address() const;
    void setAddress(const QString &address);

    QString netmask() const;
    void setNetmask(const QString &netmask);

    QString gateway() const;
    void setGateway(const QString &gateway);

private:
    Q_DISABLE_COPY(IPV4Data)
    Q_DECLARE_PRIVATE(IPV4Data);
    QScopedPointer<IPV4DataPrivate> d_ptr;

};

class IPV6DataPrivate;
class IPV6Data : public ConfigurableObject
{
    Q_OBJECT
    Q_PROPERTY(QString Method READ method WRITE setMethod)
    Q_PROPERTY(QString Address READ address WRITE setAddress)
    Q_PROPERTY(QString PrefixLength READ prefixLength WRITE setPrefixLength)
    Q_PROPERTY(QString Gateway READ gateway WRITE setGateway)
    Q_PROPERTY(QString Privacy READ privacy WRITE setPrivacy)
public:
    explicit IPV6Data(Service *parent);
    ~IPV6Data();

    QString method() const;
    void setMethod(const QString &method);

    QString address() const;
    void setAddress(const QString &address);

    QString prefixLength() const;
    void setPrefixLength(const QString &prefixLength);

    QString gateway() const;
    void setGateway(const QString &gateway);

    QString privacy() const;
    void setPrivacy(const QString &privacy);

private:
    Q_DISABLE_COPY(IPV6Data)
    Q_DECLARE_PRIVATE(IPV6Data);
    QScopedPointer<IPV6DataPrivate> d_ptr;

};

class ProxyDataPrivate;
class ProxyData : public ConfigurableObject
{
    Q_OBJECT
    Q_PROPERTY(QString Method READ method WRITE setMethod)
    Q_PROPERTY(QString URL READ url WRITE setUrl)
    Q_PROPERTY(QStringList Servers READ servers WRITE setServers)
    Q_PROPERTY(QStringList Excludes READ excludes WRITE setExcludes)
public:
    explicit ProxyData(Service *parent);
    ~ProxyData();

    QString method() const;
    void setMethod(const QString &method);

    QString url() const;
    void setUrl(const QString &url);

    QStringList servers() const;
    void setServers(const QStringList &servers);

    QStringList excludes() const;
    void setExcludes(const QStringList &excludes);

private:
    Q_DISABLE_COPY(ProxyData)
    Q_DECLARE_PRIVATE(ProxyData);
    QScopedPointer<ProxyDataPrivate> d_ptr;

};

class EthernetDataPrivate;
class EthernetData : public ConfigurableObject
{
    Q_OBJECT
    Q_PROPERTY(QString Method READ method WRITE setMethod)
    Q_PROPERTY(QString Interface READ interface WRITE setInterface)
    Q_PROPERTY(QString Address READ address WRITE setAddress)
    Q_PROPERTY(quint16 MTU READ mtu WRITE setMtu)
    Q_PROPERTY(quint16 Speed READ speed WRITE setSpeed)
    Q_PROPERTY(QString Duplex READ duplex WRITE setDuplex)
public:
    EthernetData(Service *parent);
    ~EthernetData();

    QString method() const;
    void setMethod(const QString &method);

    QString interface() const;
    void setInterface(const QString &interface);

    QString address() const;
    void setAddress(const QString &address);

    quint16 mtu() const;
    void setMtu(quint16 mtu);

    quint16 speed() const;
    void setSpeed(quint16 speed);

    QString duplex() const;
    void setDuplex(const QString &duplex);

private:
    Q_DISABLE_COPY(EthernetData)
    Q_DECLARE_PRIVATE(EthernetData);
    QScopedPointer<EthernetDataPrivate> d_ptr;

};

class ProviderDataPrivate;
class ProviderData : public ConfigurableObject
{
    Q_OBJECT
    Q_PROPERTY(QString Host READ host WRITE setHost)
    Q_PROPERTY(QString Domain READ domain WRITE setDomain)
    Q_PROPERTY(QString Name READ name WRITE setName)
    Q_PROPERTY(QString Type READ type WRITE setType)
public:
    explicit ProviderData(Service *parent);
    ~ProviderData();

    QString host() const;
    void setHost(const QString &host);

    QString domain() const;
    void setDomain(const QString &domain);

    QString name() const;
    void setName(const QString &name);

    QString type() const;
    void setType(const QString &type);

private:
    Q_DISABLE_COPY(ProviderData)
    Q_DECLARE_PRIVATE(ProviderData);
    QScopedPointer<ProviderDataPrivate> d_ptr;

};

class ServicePrivate;
class Service : public ConnManObject
{
    Q_OBJECT
    Q_ENUMS(ServiceState)
    Q_PROPERTY(QString State READ stateInternal WRITE setStateInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Error READ error WRITE setErrorInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Name READ name WRITE setNameInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Type READ type WRITE setTypeInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList Security READ security WRITE setSecurityInternal NOTIFY dataChanged)
    Q_PROPERTY(quint8 Strength READ strength WRITE setStrengthInternal NOTIFY dataChanged)
    Q_PROPERTY(bool Favorite READ isFavorite WRITE setFavoriteInternal NOTIFY dataChanged)
    Q_PROPERTY(bool Immutable READ isImmutable WRITE setImmutableInternal NOTIFY dataChanged)
    Q_PROPERTY(bool AutoConnect READ isAutoConnect WRITE setAutoConnectInternal NOTIFY dataChanged)
    Q_PROPERTY(bool Roaming READ isRoaming WRITE setRoamingInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList Nameservers READ nameservers WRITE setNameserversInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList NameserversConfiguration READ nameserversConfiguration WRITE setNameserversConfigurationInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList Timeservers READ timeservers WRITE setTimeserversInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList TimeserversConfiguration READ timeserversConfiguration WRITE setTimeserversConfigurationInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList Domains READ domains WRITE setDomainsInternal NOTIFY dataChanged)
    Q_PROPERTY(QStringList DomainsConfiguration READ domainsConfiguration WRITE setDomainsConfigurationInternal NOTIFY dataChanged)

    Q_PROPERTY(IPV4Data *IPv4 READ ipv4)
    Q_PROPERTY(IPV4Data *IPv4Configuration READ ipv4Configuration)
    Q_PROPERTY(IPV6Data *IPv6 READ ipv6)
    Q_PROPERTY(IPV6Data *IPv6Configuration READ ipv6Configuration)
    Q_PROPERTY(ProxyData *Proxy READ proxy)
    Q_PROPERTY(ProxyData *ProxyConfiguration READ proxyConfiguration)

    Q_PROPERTY(EthernetData *Ethernet READ ethernet)
    Q_PROPERTY(ProviderData *Provider READ provider)

public:
    Service(const ObjectPropertyData &info, QObject *parent = 0);
    ~Service();

    enum ServiceState {
        UndefinedState,
        IdleState,
        FailureState,
        AssociationState,
        ConfigurationState,
        ReadyState,
        DisconnectState,
        OnlineState
    };
    ServiceState state() const;

    QDBusObjectPath objectPath() const;
    QString error() const;
    QString name() const;
    QString type() const;
    QStringList security() const;
    quint8 strength() const;
    bool isFavorite() const;
    bool isImmutable() const;
    bool isRoaming() const;

    bool isAutoConnect() const;
    void setAutoConnect(bool autoConnect);

    QStringList nameservers() const;
    QStringList nameserversConfiguration() const;
    void setNameserversConfiguration(const QStringList &nameServers);

    QStringList timeservers() const;
    QStringList timeserversConfiguration() const;
    void setTimeserversConfiguration(const QStringList &timeServers);

    QStringList domains() const;
    QStringList domainsConfiguration() const;
    void setDomainsConfiguration(const QStringList &domains);

    IPV4Data *ipv4() const;
    IPV4Data *ipv4Configuration() const;

    IPV6Data *ipv6() const;
    IPV6Data *ipv6Configuration() const;

    ProxyData *proxy() const;
    ProxyData *proxyConfiguration() const;

    EthernetData *ethernet() const;
    ProviderData *provider() const;

Q_SIGNALS:
    void stateChanged();
    void dataChanged();

public Q_SLOTS:
    void connect();
    void disconnect();
    void remove();
    void moveBefore(Service *service);
    void moveAfter(Service *service);
    void resetCounters();

protected:
    Service(ServicePrivate *dd, QObject *parent = 0);
    Q_DECLARE_PRIVATE(Service);
    QScopedPointer<ServicePrivate> d_ptr;

private Q_SLOTS:
    void propertyChanged(const QString &property, const QDBusVariant &value);

private:
    QString stateInternal() const;
    void setStateInternal(const QString &state);
    void setAutoConnectInternal(bool autoConnect);
    void setDomainsInternal(const QStringList &domains);
    void setTimeserversInternal(const QStringList &servers);
    void setNameserversInternal(const QStringList &servers);
    void setNameserversConfigurationInternal(const QStringList &nameServers);
    void setTimeserversConfigurationInternal(const QStringList &timeServers);
    void setDomainsConfigurationInternal(const QStringList &domains);
    void setErrorInternal(const QString &error);
    void setNameInternal(const QString &name);
    void setTypeInternal(const QString &type);
    void setSecurityInternal(const QStringList &security);
    void setStrengthInternal(quint8 strength);
    void setFavoriteInternal(bool favorite);
    void setImmutableInternal(bool immutable);
    void setRoamingInternal(bool roaming);

    Q_DISABLE_COPY(Service)
    friend class ConfigurableObject;
};
Q_DECLARE_METATYPE(Service*);

class WifiServicePrivate;
class WifiService : public Service
{
    Q_OBJECT
    Q_PROPERTY(QString EAP READ eap WRITE setEapInternal)
    Q_PROPERTY(QString CACertFile READ caCertificateFile WRITE setCaCertificateFileInternal)
    Q_PROPERTY(QString ClientCertFile READ clientCertificateFile WRITE setClientCertificateFileInternal)
    Q_PROPERTY(QString PrivateKeyFile READ privateKeyFile WRITE setPrivateKeyFileInternal)
    Q_PROPERTY(QString PrivateKeyPassphrase READ privateKeyPassphrase WRITE setPrivateKeyPassphraseInternal)
    Q_PROPERTY(QString PrivateKeyPassphraseType READ privateKeyPassphraseType WRITE setPrivateKeyPassphraseTypeInternal)
    Q_PROPERTY(QString Identity READ identity WRITE setIdentityInternal)
    Q_PROPERTY(QString Phase2 READ phase2 WRITE setPhase2Internal)
    Q_PROPERTY(QString Passphrase READ passphrase WRITE setPassphraseInternal)
    Q_PROPERTY(bool Hidden READ isHidden WRITE setHiddenInternal)

public:
    WifiService(const ObjectPropertyData &info, QObject *parent = 0);
    ~WifiService();

    QString eap() const;
    void setEap(const QString &method);

    QString caCertificateFile() const;
    void setCaCertificateFile(const QString &path);

    QString clientCertificateFile() const;
    void setClientCertificateFile(const QString &path);

    QString privateKeyFile() const;
    void setPrivateKeyFile(const QString &path);

    QString privateKeyPassphrase() const;
    void setPrivateKeyPassphrase(const QString &passphrase);

    QString privateKeyPassphraseType() const;
    void setPrivateKeyPassphraseType(const QString &type);

    QString identity() const;
    void setIdentity(const QString &identity);

    QString phase2() const;
    void setPhase2(const QString &phase2);

    QString passphrase() const;
    void setPassphrase(const QString &passphrase);

    bool isHidden() const;

private:
    void setEapInternal(const QString &method);
    void setCaCertificateFileInternal(const QString &path);
    void setClientCertificateFileInternal(const QString &path);
    void setPrivateKeyFileInternal(const QString &path);
    void setPrivateKeyPassphraseInternal(const QString &passphrase);
    void setPrivateKeyPassphraseTypeInternal(const QString &type);
    void setIdentityInternal(const QString &identity);
    void setPhase2Internal(const QString &phase2);
    void setPassphraseInternal(const QString &passphrase);
    void setHiddenInternal(bool hidden);

    Q_DISABLE_COPY(WifiService)
    Q_DECLARE_PRIVATE(WifiService);
};
Q_DECLARE_METATYPE(WifiService*);

QDebug operator<<(QDebug, const Service *);
QDebug operator<<(QDebug, const ConfigurableObject *);
#endif

