/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c AgentAdaptor -a agentadaptor dbus/connman-agent.xml
 *
 * qdbusxml2cpp is Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#include "agentadaptor.h"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/*
 * Implementation of adaptor class AgentAdaptor
 */

AgentAdaptor::AgentAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

AgentAdaptor::~AgentAdaptor()
{
    // destructor
}

QStringList AgentAdaptor::alternates() const
{
    // get the value of property Alternates
    return qvariant_cast< QStringList >(parent()->property("Alternates"));
}

QString AgentAdaptor::identity() const
{
    // get the value of property Identity
    return qvariant_cast< QString >(parent()->property("Identity"));
}

QString AgentAdaptor::name() const
{
    // get the value of property Name
    return qvariant_cast< QString >(parent()->property("Name"));
}

QString AgentAdaptor::passphrase() const
{
    // get the value of property Passphrase
    return qvariant_cast< QString >(parent()->property("Passphrase"));
}

QString AgentAdaptor::password() const
{
    // get the value of property Password
    return qvariant_cast< QString >(parent()->property("Password"));
}

QString AgentAdaptor::previousPassphrase() const
{
    // get the value of property PreviousPassphrase
    return qvariant_cast< QString >(parent()->property("PreviousPassphrase"));
}

QString AgentAdaptor::requirement() const
{
    // get the value of property Requirement
    return qvariant_cast< QString >(parent()->property("Requirement"));
}

QByteArray AgentAdaptor::ssid() const
{
    // get the value of property SSID
    return qvariant_cast< QByteArray >(parent()->property("SSID"));
}

QString AgentAdaptor::type() const
{
    // get the value of property Type
    return qvariant_cast< QString >(parent()->property("Type"));
}

QString AgentAdaptor::username() const
{
    // get the value of property Username
    return qvariant_cast< QString >(parent()->property("Username"));
}

QString AgentAdaptor::value() const
{
    // get the value of property Value
    return qvariant_cast< QString >(parent()->property("Value"));
}

QString AgentAdaptor::wps() const
{
    // get the value of property WPS
    return qvariant_cast< QString >(parent()->property("WPS"));
}

void AgentAdaptor::Cancel()
{
    // handle method call net.connman.Agent.Cancel
    QMetaObject::invokeMethod(parent(), "Cancel");
}

void AgentAdaptor::Release()
{
    // handle method call net.connman.Agent.Release
    QMetaObject::invokeMethod(parent(), "Release");
}

void AgentAdaptor::ReportError(const QDBusObjectPath &service, const QString &error)
{
    // handle method call net.connman.Agent.ReportError
    QMetaObject::invokeMethod(parent(), "ReportError", Q_ARG(QDBusObjectPath, service), Q_ARG(QString, error));
}

void AgentAdaptor::RequestBrowser(const QDBusObjectPath &service, const QString &url)
{
    // handle method call net.connman.Agent.RequestBrowser
    QMetaObject::invokeMethod(parent(), "RequestBrowser", Q_ARG(QDBusObjectPath, service), Q_ARG(QString, url));
}

QVariantMap AgentAdaptor::RequestInput(const QDBusObjectPath &service, const QVariantMap &fields)
{
    // handle method call net.connman.Agent.RequestInput
    QVariantMap result;
    QMetaObject::invokeMethod(parent(), "RequestInput", Q_RETURN_ARG(QVariantMap, result), Q_ARG(QDBusObjectPath, service), Q_ARG(QVariantMap, fields));
    return result;
}

