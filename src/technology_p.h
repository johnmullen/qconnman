#ifndef TECHNOLOGY_P_H
#define TECHNOLOGY_P_H

#include <QDBusObjectPath>

class TechnologyInterface;
class TechnologyPrivate
{
public:
    TechnologyPrivate(const QDBusObjectPath &path)
        : technologyInterface(0),
          path(path),
          powered(false),
          connected(false),
          tethering(false)
    {
    }

    TechnologyInterface *technologyInterface;

    QDBusObjectPath path;
    bool powered;
    bool connected;
    QString name;
    QString type;
    bool tethering;
    QString tetheringIdentifier;
    QString tetheringPassphrase;

};

#endif
