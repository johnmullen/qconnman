#ifndef SERVICE_P_H
#define SERVICE_P_H

#include <QDBusObjectPath>
#include <QPointer>
#include <QStringList>

#include "types.h"

class IPV4DataPrivate
{
public:
    QString method;
    QString address;
    QString netmask;
    QString gateway;
};

class IPV6DataPrivate
{
public:
    QString method;
    QString address;
    QString prefixLength;
    QString gateway;
    QString privacy;
};

class ProxyDataPrivate
{
public:
    QString method;
    QString url;
    QStringList servers;
    QStringList excludes;
};

class EthernetDataPrivate
{
public:
    EthernetDataPrivate()
        : mtu(0),
          speed(0)
    {}

    QString method;
    QString interface;
    QString address;
    quint16 mtu;
    quint16 speed;
    QString duplex;
};

class ProviderDataPrivate
{
public:
    QString host;
    QString domain;
    QString name;
    QString type;
};


class Service;
class IPV4Data;
class IPV6Data;
class ProxyData;
class ProxyData;
class EthernetData;
class ProviderData;
class ServicePrivate
{
public:
    ServicePrivate(const ObjectPropertyData &info, Service *parent)
        : objectPath(info.path),
          strength(0),
          favorite(false),
          immutable(false),
          autoConnect(false),
          roaming(false),
          ipv4(0),
          ipv4Configuration(0),
          ipv6(0),
          ipv6Configuration(0),
          proxy(0),
          proxyConfiguration(0),
          ethernet(0),
          provider(0),
          q_ptr(parent)
    {
    }

    void initialize(const ObjectPropertyData &info);
    bool setConnmanProperty(const QString &property, const QVariant &value);

    QPointer<ServiceInterface> serviceInterface;

    QDBusObjectPath objectPath;
    QString state;
    QString error;
    QString name;
    QString type;
    QStringList security;
    quint8 strength;
    bool favorite;
    bool immutable;
    bool autoConnect;
    bool roaming;
    QStringList nameservers;
    QStringList nameserversConfiguration;
    QStringList timeservers;
    QStringList timeserversConfiguration;
    QStringList domains;
    QStringList domainsConfiguration;

    IPV4Data *ipv4;
    IPV4Data *ipv4Configuration;
    IPV6Data *ipv6;
    IPV6Data *ipv6Configuration;
    ProxyData *proxy;
    ProxyData *proxyConfiguration;

    EthernetData *ethernet;
    ProviderData *provider;

    Service * const q_ptr;
    Q_DECLARE_PUBLIC(Service);
};

class WifiServicePrivate : public ServicePrivate
{
public:
    WifiServicePrivate(const ObjectPropertyData &info, Service *parent)
        : ServicePrivate(info, parent),
          hidden(false)
    {}

    QString eapMethod;
    QString caCertificateFile;
    QString clientCertificateFile;
    QString privateKeyFile;
    QString privateKeyPassphrase;
    QString privateKeyPassphraseType;
    QString identity;
    QString phase2;
    QString passphrase;
    bool hidden;
};

#endif

