/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef PROVIDER_H
#define PROVIDER_H

#include <QVariantMap>

class Provider : public QVariantMap
{
public:
    QString name() const;
    void setName(const QString &name);

    QString host() const;
    void setHost(const QString &host);

    QString domain() const;
    void setDomain(const QString &domain);

protected:
    Provider();

};

class OpenConnectProvider : public Provider
{
public:
    OpenConnectProvider();

    QString cookie() const;
    void setCookie(const QString &cookie);

    QString serverCert() const;
    void setServerCert(const QString &serverCert);

private:
    static int s_openconnectProviderMetaTypeId;

};

class OpenVpnProvider : public Provider
{
public:
    OpenVpnProvider();

    QString caCert() const;
    void setCaCert(const QString &caCert);

    QString cert() const;
    void setCert(const QString &cert);

    QString key() const;
    void setKey(const QString &key);

private:
    static int s_openvpnProviderMetaTypeId;

};

class PptpProvider : public Provider
{
public:
    PptpProvider();

    QString user() const;
    void setUser(const QString &user);

    QString password() const;
    void setPassword(const QString &password);

private:
    static int s_pptpProviderMetaTypeId;

};

class L2tpProvider : public Provider
{
public:
    L2tpProvider();

    QString user() const;
    void setUser(const QString &user);

    QString password() const;
    void setPassword(const QString &password);

private:
    static int s_l2tpProviderMetaTypeId;

};

// metatype defines
template <>
struct QMetaTypeId<OpenConnectProvider> {
    enum { Defined = 1 };
    static int qt_metatype_id() { return QMetaType::type("QVariantMap"); }
};

template <>
struct QMetaTypeId<OpenVpnProvider> {
    enum { Defined = 1 };
    static int qt_metatype_id() { return QMetaType::type("QVariantMap"); }
};

template <>
struct QMetaTypeId<PptpProvider> {
    enum { Defined = 1 };
    static int qt_metatype_id() { return QMetaType::type("QVariantMap"); }
};

template <>
struct QMetaTypeId<L2tpProvider> {
    enum { Defined = 1 };
    static int qt_metatype_id() { return QMetaType::type("QVariantMap"); }
};


#endif

