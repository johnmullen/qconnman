#include <QDebug>

#include "manager.h"

#include "technologyitemwidget.h"
#include "wiredpage.h"
#include "wirelesspage.h"
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent),
      m_manager(0)
{
    setupUi(this);

    m_manager = new Manager(this);
    connect(technologyView, SIGNAL(clicked(QModelIndex)), this, SLOT(configureTechnology(QModelIndex)));
    connect(m_manager, SIGNAL(rowsInserted(QModelIndex,int,int)),
                 this, SLOT(createTechnologyItemWidgets(QModelIndex,int,int)));
    technologyView->setModel(m_manager);

    // create initial widgets
    createTechnologyItemWidgets(QModelIndex(), 0, m_manager->rowCount());
}

MainWindow::~MainWindow()
{
}

void MainWindow::configureTechnology(const QModelIndex &technology)
{
    ManagerNode *node = static_cast<ManagerNode*>(technology.internalPointer());
    if (!node || !node->isTechnology()) {
        qDebug() << "something really bad happened";
        return;
    }

    QWidget *page = m_pages.value(technology);
    if (!page) {
        Technology *technologyObject = node->object<Technology*>();
        QString technologyType = technologyObject->type().toLower();
        if (technologyType == "ethernet")
            page = new WiredPage(technology, this);
        else if (technologyType == "wifi")
	    page = new WirelessPage(technology, m_manager, this);
        else {
            qDebug() << "unsupported technology type: " << technologyType;
            return;
        }

        stackedWidget->addWidget(page);
        m_pages.insert(technology, page);
    }

    stackedWidget->setCurrentWidget(page);
}

void MainWindow::createTechnologyItemWidgets(const QModelIndex &parent, int start, int end)
{
    for (int row = start; row < end; ++row) {
        QModelIndex index = m_manager->index(row, 0, parent);
        ManagerNode *node = static_cast<ManagerNode*>(index.internalPointer());
        if (node->isTechnology()) {
            Technology *technology = node->object<Technology*>();
            technologyView->setIndexWidget(index, new TechnologyItemWidget(technology));
        }
    }
}
