/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QCoreApplication>
#include <QDebug>

#include "manager.h"

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();
    if (args.length() < 3) {
      qDebug() << "Usage: " << args.at(0) << " <service> [off|dhcp|manual <address> [netmask] [gateway]]";
      return 0;
    }
    
    Manager manager;
    QString path = QString("/net/connman/service/%1").arg(args.at(1));
    QDBusObjectPath objectPath(path);
    if (!manager.hasService(objectPath)) {
        qDebug() << "invalid service: " << args.at(1);
        return -1;
    }
    
    qDebug() << "Setting method " << args.at(2) << " for " << args.at(1);
    Service *service = manager.service(objectPath);
    service->ipv4Configuration()->setMethod(args.at(2));
    if (args.length() > 3)
        service->ipv4Configuration()->setAddress(args.at(3));
    if (args.length() > 4)
        service->ipv4Configuration()->setNetmask(args.at(4));
    if (args.length() > 5)
        service->ipv4Configuration()->setGateway(args.at(5));
    service->ipv4Configuration()->apply();
}
