/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef TRAYICONCONTROLLER_H
#define TRAYICONCONTROLLER_H

#include <QObject>
#include <QModelIndex>
#include <QSystemTrayIcon>

class Agent;
class Manager;
class ManagerMenuAction;
class QTreeView;
class QMenu;
class QPushButton;
class QSystemTrayIcon;
class TrayIconController : public QObject
{
    Q_OBJECT
public:
    explicit TrayIconController(QObject *parent = 0);
    ~TrayIconController();

private Q_SLOTS:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void createIndexWidgets(const QModelIndex &parent, int start, int end);

    void managerStateChanged();
    void managerOfflineModeChanged();

    void initializeIndexWidgets();

private:
    Manager *m_manager;
    Agent *m_agent;

    QSystemTrayIcon *m_systemTrayIcon;
    QTreeView *m_networkView;

};

#endif // TRAYICONCONTROLLER_H
