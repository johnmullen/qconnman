/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QDebug>

#include "technologywidget.h"
#include "ui_technologywidget.h"

TechnologyWidget::TechnologyWidget(Technology *technology, QWidget *parent)
    : QWidget(parent),
      ui(new Ui::TechnologyWidget),
      m_technology(technology)
{
    ui->setupUi(this);

    connect(technology, SIGNAL(dataChanged()), this, SLOT(updateTechnologyData()));
    connect(ui->isEnabled, SIGNAL(clicked()), this, SLOT(enabledClicked()));
    updateTechnologyData();
}

TechnologyWidget::~TechnologyWidget()
{
    delete ui;
}

void TechnologyWidget::updateTechnologyData()
{
    ui->label->setText(m_technology->name());
    if (m_technology->isPowered())
        ui->isEnabled->setCheckState(Qt::Checked);
    else
        ui->isEnabled->setCheckState(Qt::Unchecked);
}

void TechnologyWidget::enabledClicked()
{
    if (ui->isEnabled->checkState() == Qt::Checked) {
        m_technology->setPowered(true);
        Q_EMIT technologyEnabled();
    } else {
        m_technology->setPowered(false);
    }
}
