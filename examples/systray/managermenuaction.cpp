/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QTreeView>
#include <QHeaderView>
#include <QPushButton>

#include "manager.h"
#include "technologywidget.h"
#include "servicewidget.h"
#include "managermenuaction.h"

ManagerMenuAction::ManagerMenuAction(Manager *manager, QObject *parent)
    : QWidgetAction(parent),
      m_manager(manager),
      m_networkView(0)
{
/*
    m_networkView = new QTreeView;
    m_networkView->header()->hide();
    m_networkView->setSelectionMode(QAbstractItemView::NoSelection);
    connect(m_manager, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(createIndexWidgets(QModelIndex,int,int)));
    connect(m_manager, SIGNAL(rowsInserted(QModelIndex,int,int)), m_networkView, SLOT(expandAll()));
    connect(m_manager, SIGNAL(rowsRemoved(QModelIndex,int,int)), m_networkView, SLOT(expandAll()));
*/
}

ManagerMenuAction::~ManagerMenuAction()
{
}

void ManagerMenuAction::createIndexWidgets(const QModelIndex &parent, int start, int end)
{
    for (int row = start; row < end; row++) {
        QModelIndex idx = m_manager->index(row, 0, parent);
        ManagerNode *node = static_cast<ManagerNode*>(idx.internalPointer());
        if (node->isTechnology()) {
            TechnologyWidget *widget = new TechnologyWidget(node->object<Technology*>());
//            connect(widget, SIGNAL(technologyEnabled()), this, SLOT(handleTechnologyEnabled()));
            m_networkView->setIndexWidget(idx, widget);
        } else if (node->isService())
            m_networkView->setIndexWidget(idx, new ServiceWidget(node->object<Service*>()));
    }
}


QWidget *ManagerMenuAction::createWidget(QWidget *parent)
{
/*
    QMaemo5ValueButton *button = new QMaemo5ValueButton(m_text, parent);
    button->setValueLayout(QMaemo5ValueButton::ValueUnderTextCentered);
    QMaemo5ListPickSelector *pick = new QMaemo5ListPickSelector(button);
    button->setPickSelector(pick);
    if (m_actions) {
        QStringList sl;
        int curIdx = -1, idx = 0;
        foreach (QAction *a, m_actions->actions()) {
            sl << a->text();
    if (a->isChecked())
                curIdx = idx;
            idx++;
        }
        pick->setModel(new QStringListModel(sl));
        pick->setCurrentIndex(curIdx);
    } else {
        button->setEnabled(false);
    }
    connect(pick, SIGNAL(selected(QString)), this, SLOT(emitTriggered()));
    return button;
*/

    QPushButton *test = new QPushButton("testing", parent);
    return test;
}
