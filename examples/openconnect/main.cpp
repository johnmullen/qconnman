/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QCoreApplication>
#include <QSslConfiguration>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QEventLoop>
#include <QTimer>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#  include <QUrlQuery>
#endif
#include <QDebug>

#include "provider.h"
#include "manager.h"

QByteArray anyConnectHelper(const QUrl &host, const QString &domain,
                            const QString &user, const QString &password,
                            const QSslConfiguration &sslConfiguration = QSslConfiguration())
{
    // structure login information
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    QUrlQuery postData(host);
#else
    QUrl postData(host);
#endif
    postData.addQueryItem("username", user);
    postData.addQueryItem("group_list", domain);
    postData.addQueryItem("password", password);
    postData.addQueryItem("Login", "Login");
    postData.addQueryItem("tgroup", QString());
    postData.addQueryItem("next", QString());
    postData.addQueryItem("tgcookieset", QString());

    // create request
    QNetworkRequest request(host);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    if (!sslConfiguration.isNull())
        request.setSslConfiguration(sslConfiguration);

    // make requests and block
    QEventLoop loop;
    QNetworkAccessManager manager;

    // first request is to authenticate
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    QUrl url;
    url.setQuery(postData);
    QNetworkReply *reply = manager.post(request, url.toEncoded());
#else
    QNetworkReply *reply = manager.post(request, postData.encodedQuery());
#endif
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    QTimer::singleShot(10000, &loop, SLOT(quit()));
    loop.exec();
    if (!reply->isFinished()) {
        qDebug() << Q_FUNC_INFO << "unable to complete first request";
        reply->deleteLater();
        loop.quit();
        return QByteArray();
    }

    // second request is to get the cookie
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    reply = manager.post(request, url.toEncoded());
#else
    reply = manager.post(request, postData.encodedQuery());
#endif
    QTimer::singleShot(10000, &loop, SLOT(quit()));
    loop.exec();

    loop.quit();
    reply->deleteLater();
    if (!reply->isFinished()) {
        qDebug() << Q_FUNC_INFO << "unable to complete second request";
        return QByteArray();
    }

    // extract cookie
    QList<QNetworkCookie> cookies = manager.cookieJar()->cookiesForUrl(host);
    qDebug() << "checking cookies: ";
    foreach (QNetworkCookie cookie, cookies) {
        qDebug() << "\t" << cookie.name() << " = " << cookie.value();
        if (cookie.name() == "webvpn")
            return cookie.value();
    }

    return QByteArray();
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();
    if (args.length() < 5) {
      qDebug() << "Usage: " << args.at(0) << " <host> <domain> <user> <password>";
      return 0;
    }

    QString host = args.at(1);
    QString loginUrl = QString("https://%1/+webvpn+/index.html").arg(host);

    QString domain = args.at(2);
    QString username = args.at(3);
    QString password = args.at(4);
    QByteArray authCookie = anyConnectHelper(QUrl(loginUrl), domain, username, password);
    if (authCookie.isEmpty()) {
        qDebug() << Q_FUNC_INFO << "unable to obtain auth cookie";
        return -1;
    }

    OpenConnectProvider provider;
    provider.setHost(host);
    provider.setDomain(domain);
    provider.setName("testvpn");
    provider.setCookie(authCookie);

    Manager manager;
    QDBusObjectPath path = manager.connectProvider(provider);
    if (path.path().isEmpty() || path.path().isNull()) {
        qDebug() << Q_FUNC_INFO << "unable to connect provider";
        return -1;
    }

    qDebug() << Q_FUNC_INFO << "added vpn service at path: " << path.path();
    return 0;
}
